# hauk-py

Unofficial backend reimplementation for hauk in Python 3 using Quart. Uses pipenv.

Hauk is a FOSS live location sharing application for Android. Official backend is in PHP, and I wrote this reimplementation to avoid having to set up a LAMP stack on a container. You can find the Hauk codebae on https://github.com/bilde2910/Hauk and the app on F-Droid.

Everything works except for adoption for group share, and codebase isn't cleanest but it does the job. Literally wrote it at 3am.

## Setting up

- Make sure to --recursive clone this project, or if you already cloned without that, do `git submodule init` and `git submodule update`.
- Set up python3.9+, install pipenv.
- Copy `config.py.template` to `config.py` and edit it in whatever way you want.
- Run `pipenv shell` and then `pipenv update`

## Running

- Run `pipenv shell`.
- Run `hauk.py`.

## Development / Running the test suite

- Install [tox](https://tox.readthedocs.io/en/latest/).
- Run `tox`.
