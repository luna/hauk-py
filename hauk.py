import time
import math
import secrets
from dataclasses import dataclass
from typing import Optional, List, Union

from quart import Quart, request, jsonify, Response
from config import (
    instance_password,
    maximum_duration,
    instance_url,
    session_name_hex_length,
    dynamic_js,
    host,
    port,
)

app = Quart(__name__, static_url_path="", static_folder="Hauk/frontend")


@dataclass
class Point:
    latitude: float
    longitude: float
    timestamp: float
    speed: Optional[float]
    accuracy: Optional[float]
    provider: int = 1

    def to_list(self):
        """Represent a given location point in the Hauk API."""
        return [
            self.latitude,
            self.longitude,
            self.timestamp,
            self.provider,
            self.accuracy,
            self.speed,
        ]


@dataclass
class EncryptedPoint:
    latitude: str
    longitude: str
    timestamp: str
    speed: str
    accuracy: str
    provider: str
    iv: str

    def to_list(self):
        """Represent a given location point in the Hauk API."""
        return [
            self.iv,
            self.latitude,
            self.longitude,
            self.timestamp,
            self.provider,
            self.accuracy,
            self.speed,
        ]


@dataclass
class Session:
    id: str
    expire_timestamp: float
    update_interval: int
    points: List[Union[Point, EncryptedPoint]]

    encrypted: bool
    encryption_salt: Optional[str]


@app.before_serving
async def app_before_serving():
    app.hauk_sessions = {}
    app.hauk_names = {}


@app.after_request
async def app_after_request(resp):
    resp.headers["x-hauk-version"] = "1.6.1"
    return resp


@app.route("/")
async def root():
    return await app.send_static_file("index.html")


async def delete_session(session_name):
    print(f"Deleting session {session_name}.")
    session_id = app.hauk_sessions[session_name].id
    del app.hauk_names[session_id]  # Delete session name mapping
    del app.hauk_sessions[session_name]  # Delete session itself


@app.route("/api/create.php", methods=["POST"])
async def create():
    form_data = await request.form
    # dur: Share Duration in seconds
    # mod: ??
    # pwd: Password, may be empty string
    # ado: "Allow adoption into a group share", "0" or "1"
    # int: Update interval in seconds

    # Turn data from string into ints
    share_duration = int(form_data["dur"])
    update_interval = int(form_data["int"])

    # Check for data against limitations and such
    # NOTE: Upstream returns 200 OK for errors too
    if share_duration > maximum_duration:
        return f"Share duration too long, maximum is {maximum_duration}s."
    elif form_data["pwd"] != instance_password:
        return "Incorrect password."

    # Generate a session ID (private key) and session name (public key)
    session_id = secrets.token_hex(32)
    session_name = secrets.token_hex(session_name_hex_length).upper()

    # Calculate share expiration
    expire_timestamp = math.floor(time.time()) + share_duration

    # Generate a session and map ID to name
    app.hauk_sessions[session_name] = Session(
        session_id, expire_timestamp, update_interval, [], False, None
    )
    if form_data.get("e2e", "0") != "0":
        encryption_salt = form_data["salt"]
        app.hauk_sessions[session_name].encrypted = True
        app.hauk_sessions[session_name].encryption_salt = encryption_salt
    app.hauk_names[session_id] = session_name

    # Prepare and send the return text in a rather ugly way
    return_text = f"OK\n{session_id}\n{instance_url}?{session_name}\n{session_name}"
    return return_text


@app.route("/api/post.php", methods=["POST"])
async def post():
    form_data = await request.form

    # TODO: reduce code repetition here
    sid = form_data.get("sid", "")

    if not sid:
        return "No ID supplied."
    elif sid not in app.hauk_names:
        return "No such ID."

    session_name = app.hauk_names[sid]
    session = app.hauk_sessions[session_name]

    # TODO add validation for values
    if session.encrypted:
        # encrypted sessions do not get any server-side validation
        point = EncryptedPoint(
            form_data["lat"],
            form_data["lon"],
            form_data["time"],
            form_data["spd"],
            form_data["acc"],
            form_data["prv"],
            form_data["iv"],
        )
    else:
        point_time = float(form_data.get("time") or time.time())
        speed = form_data.get("spd")
        speed = float(speed) if speed is not None else None
        accuracy = form_data.get("acc")
        accuracy = float(accuracy) if accuracy is not None else None

        point = Point(
            float(form_data["lat"]),
            float(form_data["lon"]),
            point_time,
            1 if form_data.get("prv") == "1" else 0,
            accuracy,
            speed,
        )

    session.points.append(point)
    return f"OK\n{instance_url}?%s\n{session_name}\n"


@app.route("/api/stop.php", methods=["POST"])
async def stop():
    form_data = await request.form

    # TODO: reduce code repetition here
    sid = form_data.get("sid", "")

    if not sid:
        return "No ID supplied."
    elif sid not in app.hauk_names:
        return "No such ID."

    session_name = app.hauk_names[sid]

    await delete_session(session_name)
    return "OK"


@app.route("/api/fetch.php")
async def fetch():
    get_values = request.args

    # Get the session name and verify the existence of the it
    session_name = get_values.get("id", "")

    if not session_name:
        return "No ID supplied."
    elif session_name not in app.hauk_sessions:
        return "No such ID.", 404

    hauk_session = app.hauk_sessions[session_name]

    # Check if a session needs to be deleted due to expiration, if so do it
    if time.time() > hauk_session.expire_timestamp:
        await delete_session(session_name)
        return "No such ID.", 404

    # Prepare and send response
    points_result = hauk_session.points

    since = get_values.get("since")

    # Encrypted sessions have strings for all values since
    # they are all encrypted. We can not provide "since" functionality
    # because we can't decrypt it (for a good reason! that's the point
    # of the feature)
    if since is not None and not hauk_session.encrypted:
        # TODO this can fail, add validation for it
        since = float(since)
        points_result = []

        # TODO change this to filter() so it looks cleaner
        for point in hauk_session.points:
            if point.timestamp >= since:
                points_result.append(point)

    resp = {
        "type": 0,
        "expire": hauk_session.expire_timestamp,
        "interval": hauk_session.update_interval,
        "points": [point.to_list() for point in points_result],
        "serverTime": time.time(),
        "encrypted": hauk_session.encrypted,
        "salt": hauk_session.encryption_salt,
    }

    return jsonify(resp)


@app.route("/dynamic.js.php")
async def dynamicjs():
    # This is hell, basically.
    # Certain configuration values are loaded into FE through a dynamically
    # generated JS file.
    # In our case, we simply pull this from config.py instead of generating it
    # as it only reacts to config values and not to anything that'd actually
    # change during runtime
    resp = Response(dynamic_js)

    resp.headers["Content-Type"] = "application/javascript; charset=utf-8"
    return resp


if __name__ == "__main__":
    app.run(host=host, port=port)
