import asyncio
import os
import sys

import pytest

# become available to import hauk as it is one level above tests folder
#
# TODO: maybe we could create a mini hauk package so the tests are inside
sys.path.append(os.getcwd())
from hauk import app as real_app  # noqa: E402


@pytest.fixture(name="event_loop", scope="session")
def event_loop_fixture():
    loop = asyncio.new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(name="app", scope="session")
async def app_fixture(event_loop):
    real_app.loop = event_loop
    await real_app.startup()
    yield real_app
    async with real_app.app_context():
        await real_app.shutdown()


@pytest.fixture(name="test_cli")
def test_cli_fixture(app):
    return app.test_client()
